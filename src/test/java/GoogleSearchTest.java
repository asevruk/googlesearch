import com.codeborne.selenide.ElementsCollection;
import org.junit.Test;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;
import static org.junit.Assert.assertEquals;




public class GoogleSearchTest {
    @Test
    public void testSearchCommonFlow() {
        open("http://google.com/ncr");

        $(By.name("q")).setValue("Selenium automates browsers").pressEnter();
        searchResults.shouldHaveSize(10);
        searchResults.get(0).shouldHave(text("Selenium automates browsers"));

        searchResults.get(0).find(".r").click();
        $(".homepage").shouldBe(visible);

        assertEquals(url(), "http://www.seleniumhq.org/");
    }

    ElementsCollection searchResults = $$(".srg>.g");

}
